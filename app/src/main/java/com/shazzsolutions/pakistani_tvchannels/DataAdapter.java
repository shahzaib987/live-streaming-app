package com.shazzsolutions.pakistani_tvchannels;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<AndroidVersion> android;
    private Context context;

    String ARY_DIGITAL = "http://149.202.205.24:1935/DIGITAL/myStream/playlist.m3u8";

    String SAMAA_TV= "https://samaatr-hls.secdn.net/samaatr-iphone/play/samaatr_264k.stream/playlist.m3u8";

    String GEO_TV ="http://stream.jeem.tv/geo/geonewsofficial_sub/chunks.m3u8";

    String HUM_TV= "http://cdn.ebound.tv/tv/humtv/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMTA6MjQ6MDIgQU0maGFzaF92YWx1ZT1jMmlxWGlRK01KREVtVWx5N05ubUVnPT0mdmFsaWRtaW51dGVzPTYwJmlkPTE=";

    String NEWS92_HD= "http://92news.vdn.dstreamone.net/92newshd/92hd/playlist.m3u8";

    String NEWS92_HDPLUS ="http://92news.vdn.dstreamone.net/92newshd/92hd/chunklist_w597745317.m3u8";

    String EXPRESS_NEWS = "http://live.pockettv.com.pk:1937/live/smil:expressnewsweb.smil/playlist.m3u8";

    String ARY_NEWS = "http://158.69.229.29:1935/liveorigin/myStream/chunklist_w357199522.m3u8";

    String DUNYA_NEWS = "http://imob.dunyanews.tv:1935/live/smil:stream.smil/playlist.m3u8";

    String GEO_SUPER = "http://stream.jeem.tv/geo/geosuperofficial/chunks.m3u8";

    String ISLAM_TV = "http://wowza05.sharp-stream.com/islamtv/islamtv/playlist.m3u8";

    String METRO_ONE = "http://cdn.ebound.tv/tv/metroone/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMjo1Nzo0NSBQTSZoYXNoX3ZhbHVlPTkxZzdGWU0raHF0MVA4TmgyRWF4R1E9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String ABHTAK_NEWS= "http://cdn.ebound.tv/tv/abbtakk/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMjo0MDoxNyBQTSZoYXNoX3ZhbHVlPWExKysvQVFsSm9OeUZNeThxOHB1R0E9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String CHANNEL_5= "http://cdn.ebound.tv/tv/channelfive/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String SINDH_TV = "http://cdn.ebound.tv/tv/sindhnews/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String ATV = "http://cdn.ebound.tv/tv/atv/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String WAQT_NEWS  ="http://cdn.ebound.tv/tv/waqtnews/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String ROYAL_NEWS = "http://cdn.ebound.tv/tv/royaltv/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String KHYBER_NEWS = "http://cdn.ebound.tv/tv/khybernews/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String CITY_42NEWS = "http://cdn.ebound.tv/tv/city42/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String CAPITAL_TV= "http://cdn.ebound.tv/tv/capitaltv/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMzowNDoyOCBQTSZoYXNoX3ZhbHVlPThUVFZPTG0rbktWRVBUbmhVeFYxNGc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String MASALA_TV= "http://cdn.ebound.tv/tv/masalatv/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgODoxMzoxNSBBTSZoYXNoX3ZhbHVlPWFWb3NyUlo4S0pBZEtkQ2JBVTFUd0E9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    String AAJ_NEWS = "http://cdn.ebound.tv/tv/aajnews/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9OS81LzIwMTYgMjo0Nzo0NiBQTSZoYXNoX3ZhbHVlPUwvVndGcFhESUdvNEo0RWJSdW96THc9PSZ2YWxpZG1pbnV0ZXM9NjAmaWQ9MQ==";

    static final String KEY_VIDEO = "channel";

    public DataAdapter(Context context,ArrayList<AndroidVersion> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, final int position) {

        viewHolder.tv_android.setText(android.get(position).getAndroid_version_name());
        Picasso.with(context).load(android.get(position).getAndroid_image_url()).resize(240, 240).into(viewHolder.img_android);

      //  int position = i;

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

           if(InternetConnection.isNetworkAvailable(getApplicationContext()))

           {

               if (position == 0) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ARY DIGITAL", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   //  bundle.putSerializable("feed", android);
                   //  Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ARY_DIGITAL);
                   context.startActivity(intent);
               } else if (position == 1) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("SAMAA TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // bundle.putSerializable("feed", android);
                   //  Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, SAMAA_TV);
                   context.startActivity(intent);
               } else if (position == 2) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("GEO TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, GEO_TV);
                   context.startActivity(intent);
               } else if (position == 3) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("HUM TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, HUM_TV);
                   context.startActivity(intent);
               } else if (position == 4) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("92 NEWS HD", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, NEWS92_HD);
                   context.startActivity(intent);
               } else if (position == 5) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ARY NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ARY_NEWS);
                   context.startActivity(intent);
               } else if (position == 6) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("DUNYA NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, DUNYA_NEWS);
                   context.startActivity(intent);
               } else if (position == 7) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("GEO SUPER", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, GEO_SUPER);
                   context.startActivity(intent);
               } else if (position == 8) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ISLAM TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ISLAM_TV);
                   context.startActivity(intent);
               } else if (position == 9) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("METRO ONE", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, METRO_ONE);
                   context.startActivity(intent);
               } else if (position == 10) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ABH TAK NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ABHTAK_NEWS);
                   context.startActivity(intent);
               } else if (position == 11) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("Channel 5", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, CHANNEL_5);
                   context.startActivity(intent);
               } else if (position == 12) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("SINDH TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, SINDH_TV);
                   context.startActivity(intent);
               } else if (position == 13) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ATV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ATV);
                   context.startActivity(intent);
               } else if (position == 14) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("WAQT NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, WAQT_NEWS);
                   context.startActivity(intent);
               } else if (position == 15) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("ROYAL NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, ROYAL_NEWS);
                   context.startActivity(intent);
               } else if (position == 16) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("KHYBER NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, KHYBER_NEWS);
                   context.startActivity(intent);
               } else if (position == 17) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("CITY 42 NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, CITY_42NEWS);
                   context.startActivity(intent);
               } else if (position == 18) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("CAPITAL TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, CAPITAL_TV);
                   context.startActivity(intent);
               } else if (position == 19) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("MASALA TV", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, MASALA_TV);
                   context.startActivity(intent);
               } else if (position == 20) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("AAJ TV NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, AAJ_NEWS);
                   context.startActivity(intent);
               } else if (position == 21) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("92 NEWS HD PLUS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, NEWS92_HDPLUS);
                   context.startActivity(intent);
               } else if (position == 22) {
                   ((GoogleAnalyticsTVChannel) context).trackEvent("EXPRESS NEWS", "Click/Watch TV", "Track TV Channel Event");
                   Bundle bundle = new Bundle();
                   // Toast.makeText(context,"Click Position" + position , Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(context, WatchChannel.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   intent.putExtras(bundle);
                   intent.putExtra(KEY_VIDEO, EXPRESS_NEWS);
                   context.startActivity(intent);
               }

           }
           else
           {
               Toast.makeText(getApplicationContext(), "Kindly Check Your Internet Connection ! ", Toast.LENGTH_SHORT).show();
           }
                //RecyclerView.ViewHolder holder1 = (RecyclerView.ViewHolder) view.getTag();
//                int position = holder1.getAdapterPosition();
                //  RSSItem feedItem = contents.getItem(pos);
              //  Bundle bundle = new Bundle();
                //bundle.putSerializable("feed", contents);
                //;Toast.makeText(context,"Click Position " + position , Toast.LENGTH_SHORT).show();
            //    Intent intent = new Intent(context, Detail_Activity.class);
               // intent.putExtras(bundle);
               // intent.putExtra("pos", position);
                //intent.putExtra("category", cat);
				/*intent.putExtra("link",link);
				intent.putExtra("title",lfflTitle.getText());*/
                //feed.getItem()
               // context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_android;
        private ImageView img_android;
        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tv_android);
            img_android = (ImageView) view.findViewById(R.id.img_android);
        }
    }

}